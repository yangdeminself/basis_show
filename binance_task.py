import tomli
from traceback import format_exc
from time import time, sleep,mktime, strptime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from lib.binance import Client, ThreadedWebsocketManager, ThreadedDepthCacheManager
import os
from utility import *
from pprint import pprint
#matplotlib.use('Qt5Agg')

log=get_logger()

api_key=""
api_secret = ""

# # 设置代理
os.environ['http_proxy'] = 'http://127.0.0.1:10809'
os.environ['https_proxy'] = 'http://127.0.0.1:10809'

spot_pair_info = {}
coin_pair_info = {}
usdt_pair_info = {}

#现货、杠杆 api接口
client = Client(api_key, api_secret)

def get_cfg():
    try:
        import os
        current_path = os.path.dirname(os.path.abspath(__file__))
        script_name = os.path.basename(__file__)
        script_name_without_extension = os.path.splitext(script_name)[0]
        toml_file = os.path.join(current_path, script_name_without_extension + ".toml")

        cfg_data = None
        with open(toml_file, "rb") as f:
            cfg_data = tomli.load(f)

        return cfg_data
    except Exception as e:
        log.error(repr(e))
        log.error(format_exc())

#找到有效的交易对
def get_efftive_pair():
    #1.交易排名 前100
    #2.有现货，交割合约，永续合约
    
    #ex_all_pairs = {}
    global spot_pair_info, coin_pair_info, usdt_pair_info
    
    base_coin = ["USDT","USD","USDC","BUSD","TUSD"] # 稳定币
    try:    
        # prices = client.get_all_tickers()
        # log.info(prices)
        
        #现货、杠杆交易对
        infos_spot= client.get_exchange_info()
        log.info(F"现货、杠杆 symbol num:{len(infos_spot['symbols'])}")
        for it in infos_spot['symbols']:
            # log.info(F"现货、杠杆 symbol:{it['symbol']}, base:{it['baseAsset']},'quoteAsset':{it['quoteAsset']}")
            if it['quoteAsset'] not in spot_pair_info.keys():
                spot_pair_info[it['quoteAsset']] = []
            else:
                spot_pair_info[it['quoteAsset']].append(it['symbol'])
            
        #U本位合约
        infosU = client.futures_exchange_info()
        log.info(F"U本位合约 symbol num:{len(infosU['symbols'])}")
        for it in infosU['symbols']:
            # log.info(F"U本位 symbol:{it['symbol']}, contractType:{it['contractType']}, base:{it['baseAsset']},'quoteAsset':{it['quoteAsset']}")
            
            if it['quoteAsset'] not in usdt_pair_info.keys():
                usdt_pair_info[it['quoteAsset']] = []
            else:
                usdt_pair_info[it['quoteAsset']].append(it['symbol'])
        
        #币本位合约
        infosC = client.futures_coin_exchange_info()
        log.info(F"币本位合约 symbol num:{len(infosC['symbols'])}")
        for it in infosC['symbols']:
            # log.info(F"币本位 symbol:{it['symbol']}, contractType:{it['contractType']}, base:{it['baseAsset']},'quoteAsset':{it['quoteAsset']}")
            if it['quoteAsset'] not in coin_pair_info.keys():
                coin_pair_info[it['quoteAsset']] = []
            else:
                coin_pair_info[it['quoteAsset']].append(it['symbol'])
                
        
        fine_pair = []      
        #现货和币本位合约（全是USD结尾）
        # for coin in coin_pair_info['USD']:
        #     for bcoin in base_coin:
        #         if bcoin in spot_pair_info.keys():
        #             if coin in spot_pair_info[bcoin]:
        #                 log.info(F"spot - base coin 币种:{symbol}")
        #                 fine_pair.append(symbol)

        #现货和U本位合约（全是USDT,USDC结尾）
        for coin in usdt_pair_info["USDT"]:
            if coin in spot_pair_info["USDT"]:
                log.info(F"spot - USDT base coin 币种:{coin}")
                fine_pair.append(coin)
                
        for coin in usdt_pair_info["USDC"]:
            if coin in spot_pair_info["USDC"]:
                log.info(F"spot - USDC base coin 币种:{coin}")
                fine_pair.append(coin)

        log.info(F"有效交易对 num:{len(fine_pair)}, pairs:{fine_pair}")
        return fine_pair
        
    except Exception as e:
        log.error(repr(e))
        log.error(format_exc())

def pair_amount_range(symbols):
    try:
        # 获取交易产品最近24小时交易量信息
        items = client.futures_ticker()
        pair_amount_info = {}
        all_amount = {}
        for it in items:
            all_amount[it['symbol']] = it["quoteVolume"]
            if it['symbol'] in symbols:
                pair_amount_info[it['symbol']] = it["quoteVolume"]
                log.info(F"symbol:{it['symbol']}, amount:{it['quoteVolume']}")
            
        #按照交易量排序,从大到小，并打印交易量前十的币对
        sorted_pairs = sorted(pair_amount_info.items(), key=lambda x:x[1], reverse=True)
        log.info(F"交易量前十的币对:{sorted_pairs[:100]}")

        # for it in sorted_pairs[:100]:
        #     log.info(F"symbol:{it[0]}, amount:{it[1]}")
        
        # sorted_pairs = sorted(pair_amount_info.items(), key=lambda x:x[1], reverse=True)
        # log.info(F"交易量排序 pairs:{sorted_pairs}")
        return sorted_pairs
    except Exception as e:
        log.error(repr(e))
        log.error(format_exc())
        
def chack_pair_basis_arb(pair):
    info_spot = client.get_klines(symbol=pair, interval="1m", limit=1)
    spot_last_price = float(info_spot[0][4])
#     [[
#     1499040000000,      // 开盘时间
#     "0.01634790",       // 开盘价
#     "0.80000000",       // 最高价
#     "0.01575800",       // 最低价
#     "0.01577100",       // 收盘价(当前K线未结束的即为最新价)
#     "148976.11427815",  // 成交量
#     1499644799999,      // 收盘时间
#     "2434.19055334",    // 成交额
#     308,                // 成交笔数
#     "1756.87402397",    // 主动买入成交量
#     "28.46694368",      // 主动买入成交额
#     "17928899.62484339" // 请忽略该参数
#   ]]
    # log.info(info)
    
    
    info_fut = client.futures_klines(symbol=pair, interval="1m", limit=1) #
    fut_last_price = float(info_fut[0][4])
    # client.futures_coin_klines(pair) #binance futures coin kline
    basis = (fut_last_price - spot_last_price)/spot_last_price
    log.info(F"pair:{pair}, spot_last_price:{spot_last_price}, fut_last_price:{fut_last_price}, basis:{basis}")
    return basis

def chack_pair_rate_arb(pair):
    #1.永续合约和交割合约的结合
    #套利收益=资金费收益-交易手续费
    swap_fut_info = client.futures_coin_klines(symbol=pair, interval="1m", limit=1)
    swap_last_price = float(swap_fut_info[0][4])
    
    fut_info = client.futures_coin_klines(symbol=pair, interval="1m", limit=1)
    fut_last_price = float(fut_info[0][4])
    
    profile = swap_last_price 

    ## COIN Futures API
    now_rate = client.futures_coin_mark_price(symbol=pair)
        
    # Futures API
    now_rate = client.futures_funding_rate(symbol=pair)



    #2.永续合约和币币杠杆结合 --暂不考虑
    pass
          
def task():
    cfg = get_cfg()
    
    # fine_pair = get_efftive_pair()
    
    fine_pair = ['ETHUSDT', 'BCHUSDT', 'XRPUSDT', 'EOSUSDT', 'LTCUSDT', 'TRXUSDT', 'ETCUSDT', 'LINKUSDT', 'XLMUSDT', 'ADAUSDT', 'XMRUSDT', 'DASHUSDT', 'ZECUSDT', 'XTZUSDT', 'BNBUSDT', 
                 'ATOMUSDT', 'ONTUSDT', 'IOTAUSDT', 'BATUSDT', 'VETUSDT', 'NEOUSDT', 'QTUMUSDT', 'IOSTUSDT', 'THETAUSDT', 'ALGOUSDT', 'ZILUSDT', 'KNCUSDT', 'ZRXUSDT', 'COMPUSDT',
                 'OMGUSDT', 'DOGEUSDT', 'SXPUSDT', 'KAVAUSDT', 'BANDUSDT', 'RLCUSDT', 'WAVESUSDT', 'MKRUSDT', 'SNXUSDT', 'DOTUSDT', 'YFIUSDT', 'BALUSDT', 'CRVUSDT', 'TRBUSDT', 'RUNEUSDT', 
                 'SUSHIUSDT', 'EGLDUSDT', 'SOLUSDT', 'ICXUSDT', 'STORJUSDT', 'BLZUSDT', 'UNIUSDT', 'AVAXUSDT', 'FTMUSDT', 'ENJUSDT', 'FLMUSDT', 'RENUSDT', 'KSMUSDT', 'NEARUSDT', 'AAVEUSDT',
                 'FILUSDT', 'RSRUSDT', 'LRCUSDT', 'MATICUSDT', 'OCEANUSDT', 'CVCUSDT', 'BELUSDT', 'CTKUSDT', 'AXSUSDT', 'ALPHAUSDT', 'ZENUSDT', 'SKLUSDT', 'GRTUSDT', '1INCHUSDT', 'CHZUSDT', 
                 'SANDUSDT', 'ANKRUSDT', 'LITUSDT', 'UNFIUSDT', 'REEFUSDT', 'RVNUSDT', 'SFPUSDT', 'XEMUSDT', 'BTCSTUSDT', 'COTIUSDT', 'CHRUSDT', 'MANAUSDT', 'ALICEUSDT', 'HBARUSDT', 'ONEUSDT', 
                 'LINAUSDT', 'STMXUSDT', 'DENTUSDT', 'CELRUSDT', 'HOTUSDT', 'MTLUSDT', 'OGNUSDT', 'NKNUSDT', 'SCUSDT', 'DGBUSDT', 'BAKEUSDT', 'GTCUSDT', 'IOTXUSDT', 'RAYUSDT', 'C98USDT', 
                 'MASKUSDT', 'ATAUSDT', 'DYDXUSDT', 'GALAUSDT', 'CELOUSDT', 'ARUSDT', 'KLAYUSDT', 'ARPAUSDT', 'CTSIUSDT', 'LPTUSDT', 'ENSUSDT', 'PEOPLEUSDT', 'ROSEUSDT', 'DUSKUSDT', 
                 'FLOWUSDT', 'IMXUSDT', 'API3USDT', 'GMTUSDT', 'APEUSDT', 'WOOUSDT', 'FTTUSDT', 'JASMYUSDT', 'DARUSDT', 'OPUSDT', 'INJUSDT', 'STGUSDT', 'SPELLUSDT', 'LDOUSDT', 'CVXUSDT', 
                 'ICPUSDT', 'APTUSDT', 'QNTUSDT', 'FETUSDT', 'FXSUSDT', 'HOOKUSDT', 'MAGICUSDT', 'TUSDT', 'HIGHUSDT', 'MINAUSDT', 'ASTRUSDT', 'AGIXUSDT', 'PHBUSDT', 'GMXUSDT', 'CFXUSDT', 
                 'STXUSDT', 'BNXUSDT', 'ACHUSDT', 'SSVUSDT', 'CKBUSDT', 'PERPUSDT', 'TRUUSDT', 'LQTYUSDT', 'USDCUSDT', 'IDUSDT', 'ARBUSDT', 'JOEUSDT', 'TLMUSDT', 'AMBUSDT', 'LEVERUSDT', 
                 'RDNTUSDT', 'HFTUSDT', 'XVSUSDT', 'BLURUSDT', 'EDUUSDT', 'IDEXUSDT', 'SUIUSDT', 'UMAUSDT', 'RADUSDT', 'KEYUSDT', 'COMBOUSDT', 'NMRUSDT', 'MAVUSDT', 'MDTUSDT', 'XVGUSDT', 
                 'WLDUSDT', 'PENDLEUSDT', 'ARKMUSDT', 'AGLDUSDT', 'YGGUSDT', 'BNTUSDT', 'OXTUSDT', 'SEIUSDT', 'CYBERUSDT', 'HIFIUSDT', 'ARKUSDT', 'FRONTUSDT', 'GLMRUSDT', 'BICOUSDT', 
                 'STRAXUSDT', 'LOOMUSDT', 'BONDUSDT', 'STPTUSDT', 'WAXPUSDT', 'RIFUSDT', 'POLYXUSDT', 'GASUSDT', 'POWRUSDT', 'SLPUSDT', 'TIAUSDT', 'SNTUSDT', 'CAKEUSDT', 'MEMEUSDT', 
                 'TWTUSDT', 'ORDIUSDT', 'STEEMUSDT', 'BADGERUSDT', 'ILVUSDT', 'NTRNUSDT', 'BEAMXUSDT', 'PYTHUSDT', 'SUPERUSDT', 'USTCUSDT', 'ONGUSDT', 'JTOUSDT', '1000SATSUSDT', 'AUCTIONUSDT',
                 'ACEUSDT', 'MOVRUSDT', 'NFPUSDT', 'AIUSDT', 'XAIUSDT', 'WIFUSDT', 'MANTAUSDT', 'LSKUSDT', 'ALTUSDT', 'JUPUSDT', 'RONINUSDT', 'DYMUSDT', 'OMUSDT', 'PIXELUSDT', 'STRKUSDT', 
                 'GLMUSDT', 'PORTALUSDT', 'TONUSDT', 'AXLUSDT', 'METISUSDT', 'AEVOUSDT', 'VANRYUSDT', 'BOMEUSDT', 'ETHFIUSDT', 'ENAUSDT', 'WUSDT', 'TNSRUSDT', 'SAGAUSDT', 'TAOUSDT', 
                 'OMNIUSDT', 'REZUSDT', 'BBUSDT', 'NOTUSDT', 'IOUSDT', 'ZKUSDT', 'LISTAUSDT', 'ZROUSDT', 'RENDERUSDT', 'BANANAUSDT', 'RAREUSDT', 'GUSDT', 'SYNUSDT', 'SYSUSDT', 'VOXELUSDT', 
                 'ALPACAUSDT', 'SUNUSDT', 'VIDTUSDT', 'NULSUSDT', 'DOGSUSDT', 'MBOXUSDT', 'CHESSUSDT', 'FLUXUSDT', 'BSWUSDT', 'QUICKUSDT', 'ETHUSDC', 'SOLUSDC', 'XRPUSDC', 'DOGEUSDC', 
                 'SUIUSDC', 'LINKUSDC', 'ORDIUSDC', 'WLDUSDC', 'AVAXUSDC', 'WIFUSDC', 'BCHUSDC', 'LTCUSDC', 'NEARUSDC', 'ARBUSDC', 'NEOUSDC', 'FILUSDC', 'MATICUSDC', 'TIAUSDC', 'BOMEUSDC',
                 'ENAUSDC', 'ETHFIUSDC', 'CRVUSDC']
    
    # pair_amount_range(fine_pair)
    
    while True:
        try:
            #基差
            for info in cfg['basis']["pairs"]:
                basis = chack_pair_basis_arb(info['pair'])
                if basis > info['max'] :
                    log.info(F"pair:{info['pair']}, basis:{basis},基差大于 max:{info['max']}")
                
                if basis < info['min']:
                    log.info(F"pair:{info['pair']}, basis:{basis}, 基差小于 min:{info['min']}")
        except Exception as e:
            log.error(repr(e))
            log.error(format_exc())
        finally:
            sleep(60)



if __name__ == "__main__":
    task()