一、交易所数据

币安 获取api地址：
https://www.binance.com/zh-CN/binance-api

币安 获取k线地址：
https://www.binance.com/zh-CN/landing/data

b. 根据交易对 筛选
    PERPETUAL 永续合约
    CURRENT_MONTH 当月交割合约
    NEXT_MONTH 次月交割合约
    CURRENT_QUARTER 当季交割合约
    NEXT_QUARTER 次季交割合约
    PERPETUAL_DELIVERING 交割结算中合约

二、数据分析网站 coinglass
https://www.coinglass.com/zh

三、第三方lib
https://github.com/sammchardy/python-binance