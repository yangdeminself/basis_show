from psycopg2 import connect, extras, sql
from collections import deque
from datetime import datetime
from utility import *
from traceback import format_exc
from time import sleep
from simplejson import loads, dumps
from copy import deepcopy

log = get_logger()


def symbol_to_table(d_type, pair):
    tb_name = F"{d_type}_{pair.replace('-', '_')}"
    return tb_name


class PgsCon:
    def __init__(self, ip, port, db_name, user, passwd) -> None:
        self.con = connect(host=ip, port=port, dbname=db_name, user=user, password=passwd)
        self.cur = self.con.cursor()  # 用来在数据库Session里执行PostgreSQL命令
        self.check_db(db_name)
        self.d_t = "td"

    def check_db(self, db_name):
        self.cur.execute(sql.SQL("SELECT 1 FROM pg_database WHERE datname = %s"), [db_name])
        exists = self.cur.fetchone()

        # If the database doesn't exist, create it
        if not exists:
            self.cur.execute(sql.SQL("CREATE DATABASE {}").format(sql.Identifier(db_name)))
            log.error(F"create database {db_name}")

        log.info(F"database {db_name} already exit")

    def create_table(self, d_type, pair):
        tb_name = symbol_to_table(d_type, pair)
        try:
            if d_type == "td" or d_type == "tk":
                self.cur.execute(F'CREATE TABLE "{tb_name}" (ts bigint,side varchar(8),symbol varchar(16),tm TIMESTAMP,\
                                 price numeric(9,2),vol numeric(8,4),PRIMARY KEY(ts) )')  # ,side,price,vol
        except Exception as e:
            log.error(repr(e))
            log.error(format_exc())

        self.con.commit()

        # 设置索引 "CREATE INDEX index_name ON table_name;"
        log.info(F"create db {tb_name}")

    def check_table(self, pair, d_type="td"):
        tb_name = symbol_to_table(d_type, pair)

        self.cur.execute("select exists(select * from information_schema.tables where table_name=%s)", (tb_name,))
        if not self.cur.fetchone()[0]:
            self.create_table(d_type, pair)
        else:
            return True

    def insert_one(self, pair, tk):
        try:
            tb_name = symbol_to_table("td", pair)
            self.cur.execute(F'INSERT INTO "{tb_name}" (symbol, tm, ts, price, vol, side) VALUES({tk.symbol},\
                         {datetime.fromtimestamp(tk.timestamp)}, {int(tk.timestamp * 1000.0)}, {tk.side})')
            self.con.commit()
        except Exception as e:
            log.error(repr(e))
            log.error(format_exc())

    def batch_insert(self, pair, q_data: deque, num: int):
        tb_nm = symbol_to_table("td", pair)
        cmd = F'INSERT INTO "{tb_nm}" (symbol, tm, ts, price, vol, side) VALUES (%s, %s, %s, %s, %s, %s)\
              ON CONFLICT (ts) DO NOTHING'
        data = []
        for i in range(num):
            try:
                tk = q_data.pop()
                tm = datetime.fromtimestamp(tk.timestamp)
                ts = int(tk.timestamp * 1000.0)
                data.append([tk.symbol, tm, ts, float(tk.price), float(tk.amount), tk.side])

            except Exception as e:
                sleep(3)
                log.error(repr(e))
                log.error(format_exc())

        try:
            extras.execute_batch(self.cur, cmd, tuple(data))
            self.con.commit()
        except Exception as e:
            sleep(3)
            log.error(repr(e))
            log.error(format_exc())

    def batch_insert_bitget(self, pair, q_data: deque, num: int):
        tb_nm = symbol_to_table("td", pair)
        cmd = F'INSERT INTO "{tb_nm}" (symbol, tm, ts, price, vol, side) VALUES (%s, %s, %s, %s, %s, %s)\
              ON CONFLICT (ts) DO NOTHING'
        data = []
        # check_data={}
        for i in range(num):
            try:
                bt_data = q_data.pop()

                for it in bt_data:
                    ts = int(it[0])
                    tm = datetime.fromtimestamp(ts * 0.001)
                    td = [pair, tm, ts, float(it[1]), float(it[2]), it[3]]

                    data.append(td)

            except Exception as e:
                sleep(3)
                log.error(repr(e))
                log.error(format_exc())

        try:
            extras.execute_batch(self.cur, cmd, tuple(data))
            self.con.commit()
        except Exception as e:
            sleep(3)
            log.error(repr(e))
            log.error(format_exc())

    def delete_tb(self, pair, d_type="td"):
        tb_name = symbol_to_table(d_type, pair)
        self.cur.execute(F'DROP table "{tb_name}";')
        self.con.commit()

    def commit(self):
        self.con.commit()

    def close(self):
        self.con.close()

    def find_data(self, pair, ts_start, ts_end):
        try:
            data = []
            tb_name = symbol_to_table("td", pair)
            cmd = F'select * from "{tb_name}" where ts>={ts_start} and ts<={ts_end} order by ts;'
            self.cur.execute(cmd)

            rows = self.cur.fetchall()
            for row in rows:
                # print(F'{row[0]},{row[1]},{row[2]},{row[3]}')
                # data.append([row[0],row[0],row[4]])
                data.append(row)

            return data
        except Exception as e:
            log.error(repr(e))
            log.error(format_exc())


class PgsConGap:
    def __init__(self, ip, port, db_name, user, passwd) -> None:
        self.con = connect(host=ip, port=port, dbname=db_name, user=user, password=passwd)
        self.cur = self.con.cursor()  # 用来在数据库Session里执行PostgreSQL命令
        self.check_db(db_name)
        self.d_t = "gap"

    def check_db(self, db_name):
        self.cur.execute(sql.SQL("SELECT 1 FROM pg_database WHERE datname = %s"), [db_name])
        exists = self.cur.fetchone()

        # If the database doesn't exist, create it
        if not exists:
            self.cur.execute(sql.SQL("CREATE DATABASE {}").format(sql.Identifier(db_name)))
            log.error(F"create database {db_name}")

        log.info(F"database {db_name} already exit")

    def create_table(self, pair):
        tb_name = symbol_to_table(self.d_t, pair)
        try:
            # [lts, ts, gap, ba_pr, bg_pr,ex]
            field = "lts numeric(17,7),ts numeric(13,3),gap numeric(9,3),ba_pr numeric(9,2),bg_pr numeric(9,2),ex varchar(8),PRIMARY KEY(lts)"
            self.cur.execute(F'CREATE TABLE "{tb_name}" ({field})')
        except Exception as e:
            log.error(repr(e))
            log.error(format_exc())

        self.con.commit()

        # 设置索引 "CREATE INDEX index_name ON table_name;"
        log.info(F"create db.{tb_name}")

    def check_table(self, pair):
        tb_name = symbol_to_table(self.d_t, pair)

        self.cur.execute("select exists(select * from information_schema.tables where table_name=%s)", (tb_name,))
        if not self.cur.fetchone()[0]:
            self.create_table(pair)
        else:
            return True

    def batch_insert(self, pair, q_data: deque, num: int):
        tb_name = symbol_to_table(self.d_t, pair)
        cmd = F'INSERT INTO "{tb_name}" (lts, ts, gap, ba_pr, bg_pr,ex) VALUES (%s, %s, %s, %s, %s, %s) ON CONFLICT (lts) DO NOTHING'
        data = []
        for i in range(num):
            try:
                tk = q_data.pop()
                data.append(tk)

            except Exception as e:
                sleep(3)
                log.error(repr(e))
                log.error(format_exc())

        try:
            extras.execute_batch(self.cur, cmd, tuple(data))
            self.con.commit()
        except Exception as e:
            sleep(3)
            log.error(repr(e))
            log.error(format_exc())

    def delete_tb(self, pair):
        tb_name = symbol_to_table(self.d_t, pair)
        self.cur.execute(F'DROP table "{tb_name}";')
        self.con.commit()

    def commit(self):
        self.con.commit()

    def close(self):
        self.con.close()

    def find_data(self, pair, ts_start, ts_end):
        try:
            # data = []
            tb_name = symbol_to_table(self.d_t, pair)
            cmd = F'select * from "{tb_name}" where ts>={ts_start} and ts<={ts_end} order by ts;'
            self.cur.execute(cmd)

            return self.cur

            # rows = self.cur.fetchall()
            # for row in rows:
            #     # print(F'{row[0]},{row[1]},{row[2]},{row[3]}')
            #     # data.append([row[0],row[0],row[4]])
            #     data.append(row)

            # return data
        except Exception as e:
            log.error(repr(e))
            log.error(format_exc())


def test():
    pass


if __name__ == "__main__":
    test()
