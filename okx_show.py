import tomli
from traceback import format_exc
from time import time, sleep,mktime, strptime
import okx.PublicData as PublicData
import okx.MarketData as MarketData
import matplotlib.pyplot as plt
from okx import SpreadTrading

from utility import *

#pip install python-okx

spot_price_list = []
fut_price_list = []

log=get_logger()

def get_cfg():
    try:
        import os
        current_path = os.path.dirname(os.path.abspath(__file__))
        script_name = os.path.basename(__file__)
        script_name_without_extension = os.path.splitext(script_name)[0]
        toml_file = os.path.join(current_path, script_name_without_extension + ".toml")

        cfg_data = None
        with open(toml_file, "rb") as f:
            cfg_data = tomli.load(f)

        return cfg_data
    except Exception as e:
        log.error(repr(e))
        log.error(format_exc())

def plot_kline(spot_price_list, fut_price_list, diff):

    # plt.plot(spot_price_list, label='spot')      # 绘制现货价格
    
    # plt.plot(fut_price_list, label='fut')        # 绘制期货价格
    
    plt.plot(diff, label='diff')                 # 绘制价差
    
    plt.legend()                                  # 显示图例    
    plt.show()                                    # 显示图像    
    pass

def test_fut_volume():
    publicDataAPI = PublicData.PublicAPI(flag="0",debug = False)
    sym = "BTC-USD"
    sym_list = ["240419","240426","240531","240628","240927","241227"]
    
    for code in sym_list:
        symbol = sym + "-" + code    
       
        # 获取单个交易产品的市场的持仓总量
        result = publicDataAPI.get_open_interest(instType="FUTURES", instId=symbol)
        print(result)
        #'oi': '2453224', 持仓量（按张折算）
        # 'oiCcy': '3839.5818008232513734',持仓量（按币折算）
    
    #获取现货 历史k线数据
    marketDataAPI = MarketData.MarketAPI(flag="0",debug = False)
    for code in sym_list:
        symbol = sym + "-" + code    
        result = marketDataAPI.get_history_candlesticks(instId=symbol, before= int(1712937600000), after=int(1713024000000), bar="1D")
        print(result)
        
    # api_key = 'your_apiKey'
    # api_secret_key = 'your_secretKey'
    # passphrase = 'your_secretKey'
    # tradeApi = SpreadTrading.SpreadTradingAPI(api_key, api_secret_key, passphrase, False, '1')
    # tradeApi.get_public_trades(sprdId='ETH-USDT-SWAP_ETH-USDT-230929')
        


def task():
    cfg = get_cfg()
    test_fut_volume()
    #
    symbol_spot = cfg['global']["spot"]
    if cfg['global']["fut"] != "":
        symbol2 = cfg['global']["fut"]
    else:
        symbol2 = cfg['global']["swap"]
        
    # "yyyy-mm-dd hh:mm:ss" 格式时间转时间戳    
   
    start = mktime(strptime(cfg['global']["start"], "%Y-%m-%d %H:%M:%S"))*1000
    end = mktime(strptime(cfg['global']["end"], "%Y-%m-%d %H:%M:%S"))*1000


    kline_type = cfg['global']["bar"]
    try:
        #获取现货 历史k线数据
        marketDataAPI = MarketData.MarketAPI(flag="0")

        # 获取交易产品历史K线数据
        result = marketDataAPI.get_history_candlesticks(instId=symbol_spot, before=int(start), after=int(end), bar=kline_type)
        if result['code'] != "0":
            return False
        else:           
            data_list = result['data']
            for it in data_list:
                # ts 开始时间;       o 开盘价;  h 最高价;  l 最低价;  c 收盘价;     v 成交量;      p 成交额;      t 时间戳;   confirm	String	K线状态
                # ['1713020400000', '67554.9', '67928.4', '67430.5', '67672.1', '305.64841801', '20705179.395703591', '20705179.395703591', '1'
                price = float(it[4])
                spot_price_list.append(price)
            
        print(result)
        
        # 获取交易产品历史K线数据
        result = marketDataAPI.get_history_candlesticks(instId=symbol2, before= int(start), after=int(end), bar=kline_type)
        print(result)
        if result['code'] != "0":
            return False
        else:           
            data_list = result['data']
            for it in data_list:
                # ts 开始时间;       o 开盘价;  h 最高价;  l 最低价;  c 收盘价;     v 成交量;      p 成交额;      t 时间戳;   confirm	String	K线状态
                #['1713020400000', '67554.9', '67928.4', '67430.5', '67672.1', '305.64841801', '20705179.395703591', '20705179.395703591', '1'
                price = float(it[4])
                fut_price_list.append(price)
                
        diff = []
        for i in range(len(spot_price_list)):
            diff.append(fut_price_list[i]-spot_price_list[i])
        
        print(diff)
        
        plot_kline(spot_price_list, fut_price_list, diff)
        
        #publicDataAPI = PublicData.PublicAPI(flag="0")
        # # 获取交易产品基础信息
        # result = publicDataAPI.get_instruments(instType="FUTURES", instId=symbol2)
        # print(result)


    except Exception as e:
        log.error(repr(e))
        log.error(format_exc())



if __name__ == "__main__":
    task()