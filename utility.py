import asyncio
from datetime import datetime
import errno
import json
import logging
import logging.handlers
import os
import traceback
import requests

import hashlib
import math

from os import makedirs,getcwd
from sys import exit, path,argv

import json

phoneKey = "key-1c7411a7ddb9c76d0c739636a715e5f8"
def sendCall(phone,warnInfo):
    try:
        resp = requests.post(R"http://voice-api.luosimao.com/v1/verify.json", auth=("api", phoneKey),
                                data={"mobile": phone,"code": warnInfo},
                                timeout=10, 
                                verify=False)

        result = json.loads(resp.content)
    except Exception as e:
        logging.error(repr(e))
        print(repr(e))
        logging.error(traceback.format_exc())
        print('traceback:\n%s' % traceback.format_exc())

    print("call "+phone+" :"+warnInfo)
    logging.info("call "+phone+" :"+warnInfo)
    logging.info(result)
    print(result)
    return result


baseUrl = R"https://qyapi.weixin.qq.com/cgi-bin/webhook/send"
#keyNath = R"7b717f40-d5a8-46ee-a341-b298c6b8c710"
keyDelin = R"08da2fb5-face-4ba4-8243-3c070e1b6dd6"

selfMatchKey = "52e20f50-0f5f-4c24-bacc-1675a1d942d3"

def sendWxMsg(key=keyDelin, msg=""):
    try:
        tt = str(datetime.datetime.now())
        testData = {
            "msgtype": "text",
            "text": {
                "content": F"时间:{tt[:19]}\n{msg}",
            }
        }

        fullUrl = F"{baseUrl}?key={key}"
        #print(fullUrl)
        r = requests.post(url=fullUrl, json=testData)
        logging.info(F"{testData},rst:{r}")
        #print(r)
    except Exception as e:
        logging.error(repr(e))
        print(repr(e))
        logging.error(traceback.format_exc())
        print('traceback:\n%s' % traceback.format_exc())

def sendSmMsg(key=selfMatchKey, msg=""):
    try:
        testData = {
            "msgtype": "text",
            "text": {
                "content": str(datetime.datetime.now())+"\n" + msg,
            }
        }

        fullUrl = baseUrl + R"?key="+key
        #print(fullUrl)
        r = requests.post(url=fullUrl, json=testData)
        logging.info(F"{testData},rst:{r}")
        #print(r)
    except Exception as e:
        logging.error(repr(e))
        print(repr(e))
        logging.error(traceback.format_exc())
        print('traceback:\n%s' % traceback.format_exc())

def log_init(proj_path=".") -> object:
    try:
        makedirs(F"{proj_path}/logs", exist_ok=True)  # Python>3.2
    except TypeError:
        try:
            makedirs(F"{proj_path}/logs")
        except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and path.isdir(F"{proj_path}/logs"):
                pass
            else:
                raise

    # log_format = logging.Formatter("%(asctime)s-%(threadName)s-%(levelname)s- %(message)s [%(filename)s:%(lineno)s]")
    log_format = logging.Formatter("%(message)s")
    file_name = datetime.now().strftime("%Y%m%d-%H%M%S") + '.log'

    # logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)

    logger = logging.getLogger(name="s")
    logger.setLevel(logging.INFO)

    # file handler
    fh = logging.FileHandler(filename=F"{proj_path}/logs/" + file_name, encoding="utf-8", mode="a")
    fh.setFormatter(log_format)
    fh.setLevel(logging.INFO)

    console_format = logging.Formatter("%(asctime)s %(message)s [%(filename)s:%(lineno)s]")
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    ch.setFormatter(console_format)

    logger.addHandler(fh)
    logger.addHandler(ch)
    print("log init")
    return logger

def get_logger():
    return logging.getLogger('s')

log = None
print("log import")

if log is None:
    log = log_init()

def checkMD5(src,chkHash) -> bool:
    # print("src: " + src)
    # print("chkHash: " + chkHash)
    srcHash = hashlib.md5(src.encode("utf8")).hexdigest()
    if srcHash == chkHash:
        # print("srcHash: " + srcHash)
        return True
    else:
        print("srcHash: " + srcHash)
        return False

#btc-usdt --> btcusdt
def pairToOurName(iniName:str):
    if -1 == iniName.find("-"):
        return ""

    coin = iniName.split("-")[0]
    baseCoin = iniName.split("-")[1]
    return F"{coin.lower()}-{baseCoin.lower()}"

#btc-usdt --> BTC/USDT
def pairToPubName(iniName:str):
    if -1 == iniName.find("-"):
        return ""
    coin = iniName.split("-")[0]
    baseCoin = iniName.split("-")[1]
    return F"{coin.upper()}/{baseCoin.upper()}"

if __name__ == "__main__":
    #sendCall("17005486688", "999999")
    #sendWxMsg(keyDelin, "老王醒醒吧")
    #sendWxMsg(keyNath, "testData")
    sendWxMsg(selfMatchKey, "以后自成交出问题，就在这个群里发信息")
